package com.example.appsoma;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private EditText Numero1, Numero2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Numero1 = (EditText) (findViewById(R.id.n1));
        Numero2 = (EditText) (findViewById(R.id.n2));
        Button btnSomar = (Button) (findViewById(R.id.btnSomar));
        btnSomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n1 = Integer.parseInt(Numero1.getText().toString());
                int n2 = Integer.parseInt(Numero2.getText().toString());
                int soma = n1 + n2;
                Toast.makeText(MainActivity.this, "Resultado: " +soma, Toast.LENGTH_LONG).show();
            }
        });

    }
}
